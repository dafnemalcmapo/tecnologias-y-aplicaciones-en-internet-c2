const express = require('express');
const http = require('http');
const puerto = 80;
const json = require('body-parser');

const app = express();

app.set("view engine", "ejs");
app.use(json.urlencoded({extends:true}))

app.set(express.static(__dirname + '/public'));

app.get('/', (req, res) =>{
    res.render('index', {titulo: "Mi primer página en EJS",
    nombre: "Malcampo Virgen Dafne Jaqueline"});
});

app.get('/tabla', (req, res) =>{

    const params = {
        numero : req.query.numero
    }

    res.render('tabla', params);
});

app.post('/tabla', (req, res) =>{

    const params = {
        numero : req.body.numero
    }

    res.render('tabla', params);
});


app.listen(puerto, () =>{
    console.log("Iniciando el servidor por el puerto " + puerto);
});